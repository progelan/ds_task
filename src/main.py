import os
import matplotlib.pyplot as plt
import pandas as pd
from collections import Counter
import seaborn as sns

from extractor import get_entities
from normalizer import make_normal_dict, normalize_entities
from separator import separate

data = {
    'id': [],
    'text': [],
    'entities': [],
    'entities_more_entry': []
}

freq_data = {
    'entity': [],
    'frequency': []
}


def plot_freq_hist():
    df = pd.DataFrame(data=freq_data)

    sns.set_color_codes("pastel")
    sns.barplot(x="frequency", y="entity", data=df)
    sns.despine(left=True, bottom=True)

    plt.tight_layout()
    plt.savefig('result.png', dpi=300)


def main():
    os.chdir(os.environ['SOURCE_DIR'])
    df = pd.read_csv('interfax.csv.gz', nrows=int(os.environ['SOURCE_ROWS_NUMBER']))

    print('working...')

    for idx, row in df.iterrows():
        paragraphs = separate(row['text'])
        normal_dict = make_normal_dict(row['text'])

        entities = normalize_entities(get_entities(paragraphs), normal_dict)

        counts = Counter(entities)
        more_number = int(os.environ['MORE_ENTRY_NUMBER'])
        entities_more_entry = [ety for ety in counts if counts[ety] > more_number]

        if len(entities_more_entry) > 0:
            data['id'].append(idx)
            data['text'].append(row['text'])
            data['entities'].append(entities)
            data['entities_more_entry'].append(entities_more_entry)

            for entity in entities_more_entry:
                freq_data['entity'].append(entity)
                freq_data['frequency'].append(counts[entity])

    result_df = pd.DataFrame(data=data)

    os.chdir('../../output')
    result_df.to_csv('result.csv')


if __name__ == '__main__':
    main()
    plot_freq_hist()
    print('script successful executed, please open directory ', os.path.abspath(os.path.curdir))
