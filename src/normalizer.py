from natasha import (
    Segmenter,
    NewsEmbedding,
    NewsMorphTagger,
    NewsSyntaxParser,
    Doc,
    MorphVocab,
    NewsNERTagger
)

segmenter = Segmenter()
emb = NewsEmbedding()
morph_tagger = NewsMorphTagger(emb)
syntax_parser = NewsSyntaxParser(emb)
ner_tagger = NewsNERTagger(emb)
morph_vocab = MorphVocab()


def make_normal_dict(text):
    doc = Doc(text)

    doc.segment(segmenter)
    doc.tag_morph(morph_tagger)
    doc.parse_syntax(syntax_parser)
    doc.tag_ner(ner_tagger)

    for span in doc.spans:
        span.normalize(morph_vocab)

    return {_.text: _.normal for _ in doc.spans}


def normalize_entities(raw_entities, entity_dict):
    entities = []
    for entity in raw_entities:
        if entity in entity_dict:
            entities.append(entity_dict[entity])
        else:
            entities.append(entity)
    return entities
