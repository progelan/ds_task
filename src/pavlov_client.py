import json
import os
import requests


def send_for_recognition(paragraph):
    try:
        data = json.dumps({"x": [f"{paragraph}"]})
        headers = {'Content-Type': 'application/json; charset=utf-8'}
        response = requests.post(os.environ['DPAVLOV_API_URL'], data=data, headers=headers)

        return response.json()
    except requests.exceptions.RequestException as e:
        print(e)
