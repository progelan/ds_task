import os
from nltk.tokenize import sent_tokenize
import nltk
nltk.download('punkt')

limit_chars = int(os.environ['SEP_CHARS_LIMIT'])


def separate(text):
    sentences = sent_tokenize(text)

    paragraphs = []
    paragraph = ''

    for sentence in sentences:
        paragraph = paragraph + sentence
        if len(paragraph) > limit_chars:
            paragraphs.append(paragraph)
            paragraph = ''

    if len(paragraph) > 0:
        paragraphs.append(paragraph)

    return paragraphs
