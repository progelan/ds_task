from pavlov_client import send_for_recognition


def get_entities(paragraphs):
    entities = []
    for paragraph in paragraphs:
        recognition = send_for_recognition(paragraph)

        source = recognition[0][0]
        conditions = recognition[0][1]

        start_idx = None
        for idx, val in enumerate(conditions):
            if val != 'O':
                entity = source[idx]

                if val in ['B-LOC', 'B-ORG', 'B-PER']:
                    start_idx = len(entities)
                    entities.append(entity)
                elif val in ['I-LOC', 'I-ORG', 'I-PER']:
                    if start_idx is None:
                        entities.append(entity)
                    else:
                        entities[start_idx] = entities[start_idx] + f' {entity}'
            else:
                start_idx = None

    return entities
