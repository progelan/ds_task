FROM python:3.8-slim-buster

WORKDIR /ds_task

COPY requirements.txt requirements.txt
RUN pip3 install -r requirements.txt

COPY . .

ENV DPAVLOV_API_URL=https://7004.lnsigo.mipt.ru/model \
    SOURCE_DIR=source \
    SOURCE_ROWS_NUMBER=100 \
    MORE_ENTRY_NUMBER=5 \
    SEP_CHARS_LIMIT=500

RUN mkdir -p /ds_task/source
ADD https://github.com/ods-ai-ml4sg/proj_news_viz/releases/download/data/interfax.csv.gz /ds_task/source

CMD [ "python3", "./app/main.py" ]
