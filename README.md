```
# сборка образа
docker build -t ds_task .
```

```
# запуск скрипта в контейнере
docker run -it -v $(pwd)/output:/output ds_task
```


#### Openshift CI
```shell
oc login -u kubeadmin https://api.crc.testing:6443
GL_OPERATOR_VERSION=0.27.1
PLATFORM=openshift
kubectl create namespace gitlab-system
kubectl apply -f https://gitlab.com/api/v4/projects/18899486/packages/generic/gitlab-operator/${GL_OPERATOR_VERSION}/gitlab-operator-${PLATFORM}-${GL_OPERATOR_VERSION}.yaml
```

#### Docker CI 
```shell
docker run -d \
      --name gitlab-runner \
      --restart always \
      -v /var/run/docker.sock:/var/run/docker.sock \
      -v gitlab-runner-config:/etc/gitlab-runner \
      gitlab/gitlab-runner:latest
```

```shell
docker volume inspect gitlab-runner-config
sudo cat /var/lib/docker/volumes/gitlab-runner-config/_data/config.toml
```
